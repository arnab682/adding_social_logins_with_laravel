<?php

namespace App\Http\Controllers;
//use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Hash;
use Socialite;
use Str;
use App\User;

class SignInController extends Controller
{
    public function index(){
        return view('pages.login');
    }

    public function post(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password'=> 'required'
        ]);
            //dd($request);
        $credentials = $request->only(['email', 'password']);
        if(Auth::attempt($credentials)){
            return redirect()->intended('/deshboard');
        }

        return redirect()->back()
            ->withErrors(['email'=>'Incorrect email/password combination','password'=>'Incorrect email/password combination']);
    }

    public function logout(){
        Auth::logout();

        return redirect('/');
    }

    public function github(){
        //send the user's request to github

        return Socialite::driver('github')->redirect();
    }

    public function githubRedirect(){
        //get Oauth request back from github to authenticate user
        $user = Socialite::driver('github')->stateless()->user();
        //dd($user);

        //if this user doesn't exist, add them
        //if they do, get the model
        //either way, authenticate the user into the application and redirect afterwards
        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name'     => $user->name,
            'password' => Hash::make(Str::random(24))
        ]);
        //dd($user);
        Auth::login($user, true);

        return redirect('/deshboard');
    }

    public function facebook(){
        //send the user's request to github

        return Socialite::driver('facebook')->redirect();
    }

    public function facebookRedirect(){
        //get Oauth request back from github to authenticate user
        $user = Socialite::driver('facebbok')->stateless()->user();
        dd($user);

        //if this user doesn't exist, add them
        //if they do, get the model
        //either way, authenticate the user into the application and redirect afterwards
        $user = User::firstOrCreate([
            'email' => $user->email
        ], [
            'name'     => $user->name,
            'password' => Hash::make(Str::random(24))
        ]);
        //dd($user);
        Auth::login($user, true);

        return redirect('/deshboard');
    }
}
