<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::view('/', 'welcome');
Route::group(['middleware'=>'guest'], function(){
    Route::get('/sign-in', 'SignInController@index');

    Route::post('/sign-in', 'SignInController@post');

    Route::get('/sign-in/github', 'SignInController@github');

    Route::get('/sign-in/github/redirect', 'SignInController@githubRedirect');

    Route::get('/sign-in/facebook', 'SignInController@facebook');

    Route::get('/sign-in/facebook/redirect', 'SignInController@facebookRedirect');
});

Route::group(['middleware'=>'auth'], function(){
    Route::view('/deshboard', 'pages.home');

    Route::get('/sign-out', 'SignInController@logout');
});


//Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
