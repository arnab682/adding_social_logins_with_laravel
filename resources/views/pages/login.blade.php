<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div class="container">
    <form action="{{url('/sign-in')}}" method="post" autocomplete="off">
      <h3>Sign In</h3>
      <div class="from-row mb-2">
        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
        @if($errors->first('email'))
          <p class="text-danger">{{$errors->first('email')}}</p>
        @endif
      </div>
      <div class="from-row mb-2">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        @if($errors->first('password'))
          <p class="text-danger">{{$errors->first('password')}}</p>
        @endif
      </div>
      <div class="form-row">
        {{csrf_field()}}
        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
      </div>
      <div class="form-row">
        <a href="{{url('/sign-in/github')}}" type="submit" class="btn btn-secondary btn-block mt-2">Sign In with Github</a>
      </div>
      <div class="form-row">
        <a href="{{url('/sign-in/facebook')}}" type="submit" class="btn btn-linler btn-block mt-2">Sign In with Facebook</a>
      </div>
    </form>
  </div>
</body>
</html>
